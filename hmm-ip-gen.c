/*
 * Copyright (C) 2017 Hasan Redzovic, Aleksandra Smiljanic. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "hmm-ip-gen.h"
#include <string.h>
#include <time.h>

struct gs{

	long double *gsi;
	long double **gsij;

};

struct lambda{
	long double **A;
	long double **B;
	long double **P;
	long double **at;
	long double **bt;
	long double *ct;
	int N;
	int T;
	int M;
	int *ob;
	struct gs *gs;
};

struct lambda lm_init(int N, int T, int M){

	long double **A, **B, **P, **at, **bt, *ct;
	int i, j, *ob;

	struct gs *gs;
	struct lambda triplet;

	A = (long double **)malloc(sizeof(long double*)*N);
	B = (long double **)malloc(sizeof(long double*)*N);
	P = (long double **)malloc(sizeof(long double*)*1);

	at = (long double **)malloc(sizeof(long double*)*T);
	bt = (long double **)malloc(sizeof(long double*)*T);


	ct = (long double *)malloc(sizeof(long double)*T);

	ob = (int *)malloc(sizeof(long double)*T);

	gs = (struct gs*)malloc(sizeof(struct gs)*T);

	for(i=0;i<N;i++){

		A[i] = (long double *)malloc(sizeof(long double)*N);
		bzero(A[i],sizeof(long double)*N);

		B[i] = (long double *)malloc(sizeof(long double)*M);
		bzero(B[i],sizeof(long double)*M);
	}

	P[0] = (long double *)malloc(sizeof(long double)*N);
	bzero(P[0],sizeof(long double)*N);

	for(i=0;i<T;i++){

		at[i] = (long double *)malloc(sizeof(long double)*N);
		bzero(at[i],sizeof(long double)*N);

		bt[i] = (long double *)malloc(sizeof(long double)*N);
		bzero(bt[i],sizeof(long double)*N);

		gs[i].gsi = (long double *)malloc(sizeof(long double)*N);
		gs[i].gsij = (long double **)malloc(sizeof(long double *)*N);

		for(j=0;j<N;j++){

			gs[i].gsij[j] = (long double *)malloc(sizeof(long double)*N);
		}

	}

	bzero(ct,sizeof(long double)*T);
	bzero(ob,sizeof(long double)*T);

	triplet.A = A;
	triplet.B = B;
	triplet.P = P;
	triplet.N = N;
	triplet.T = T;
	triplet.M = M;
	triplet.ob = ob;
	triplet.at = at;
	triplet.ct = ct;
	triplet.bt = bt;
	triplet.gs = gs;

	return triplet;
}

void rnd_init(long double **matrix, int rows, int columns){

	long double rnd_offset, rnd_offset_sum;
	int i,j;

	for(i=0;i<rows;i++){

		rnd_offset_sum=0;

		for(j=0;j<columns;j++){

			if(j==(columns-1)){
				matrix[i][j]= 1 - rnd_offset_sum;
			}else{
				rnd_offset = ((long double) rand()/(RAND_MAX))/ (columns*10000);
				matrix[i][j]=((long double) 1/columns)+rnd_offset;
				rnd_offset_sum += ((long double) 1/columns) + rnd_offset;
			}
		}
	}
}

struct br_vls rnd_seq(struct lambda lm_size, struct lambda lm_time, char *dev_name){

	int *ob_size = lm_size.ob;
	int *ob_time = lm_time.ob;
	int T = lm_size.T;
	int M_size = lm_size.M;
	int M_time = lm_time.M;
	struct br_vls bord;
	bord = sniffer(dev_name, T, ob_size, ob_time, M_size, M_time);
	return bord;
}

void rnd_lm(struct lambda lm){

    rnd_init(lm.A,lm.N,lm.N);
	rnd_init(lm.B,lm.N,lm.M);
	rnd_init(lm.P,1,lm.N);

}

void write_lambda(long double **matrix, int rows, int columns){

	int i,j;
	char write_temp[50];
	char write_buff[50*columns];

	for(i=0;i<rows;i++){

			memset(write_buff, 0, sizeof(write_buff));
			for(j=0;j<columns;j++){
				memset(write_temp, 0, sizeof(write_temp));
				snprintf(write_temp, sizeof(write_temp), "%.15Lf ", matrix[i][j]);
				if(j==0){
					strcpy(write_buff,write_temp);
				}else{
					strcat(write_buff, write_temp);
				}

			}
			printf("%s \n",write_buff);
		}
}

void write_seq(int *par_1, int sz_1, int *par_2, int sz_2, int *par_3, int sz_3, int *par_4, int sz_4, int T, int T_br, char *file_path){

	int fp, i, m;
    char write_buff[250];
    char write_temp[50];
    ssize_t r;

    m = sz_1;

    if(m<sz_2)
    	m = sz_2;
    if(m<sz_3)
    	m = sz_3;
    if(m<sz_4)
       	m = sz_4;

	for(i=0;i<m;i++){

		memset(write_buff, 0, sizeof(write_buff));

		memset(write_temp, 0, sizeof(write_temp));
		if(i>=sz_1){
			snprintf(write_temp, sizeof(write_temp), "0 \t");
		}else{
			snprintf(write_temp, sizeof(write_temp), "%0.20Lf \t",(long double)100*par_1[i]/T);
		}
		strcpy(write_buff,write_temp);

		memset(write_temp, 0, sizeof(write_temp));
		if(i>=sz_2){
			snprintf(write_temp, sizeof(write_temp), "0 \t");
		}else{
			snprintf(write_temp, sizeof(write_temp), "%0.20Lf \t", (long double)100*par_2[i]/T);
		}
		strcat(write_buff, write_temp);

		memset(write_temp, 0, sizeof(write_temp));
		if(i>=sz_3){
			snprintf(write_temp, sizeof(write_temp), "0 \t");
		}else{
			snprintf(write_temp, sizeof(write_temp), "%0.20Lf \t",(long double)100*par_3[i]/T_br);
		}
		strcat(write_buff, write_temp);

		memset(write_temp, 0, sizeof(write_temp));
		if(i>=sz_4){
			snprintf(write_temp, sizeof(write_temp), "0 \n");
		}else{
			snprintf(write_temp, sizeof(write_temp), "%d \n", par_4[i]);
		}
		strcat(write_buff, write_temp);

		fp = open(file_path,(O_CREAT | O_WRONLY | O_APPEND),00644);
		r = write(fp, write_buff, strlen(write_buff));

		if(r==-1){
			printf("Failed to write!! \n");
		}

		close(fp);
	}

}

void result_pars(int *input, int size, int *output){

	int i;

	for(i=0;i<size;i++){
		output[input[i]]++;
	}
}

void a_pass(long double **at, long double *ct, int *ob, int T, long double **A, long double **B, long double **P, int N){

	int i,j,t;

    //compute a[0](i)
	ct[0] = 0;

    for(i=0;i<N;i++){

    	at[0][i] = P[0][i]*B[i][ob[0]];
		ct[0] +=  at[0][i];
	}

    //scale a0(i)
    ct[0]=1/ct[0];
    for(i=0;i<N;i++){

    	at[0][i] *= ct[0];
	}

    //compute at(i)
    for(t=1;t<T;t++){

    	ct[t] = 0;
    	for(i=0;i<N;i++){

    		at[t][i] = 0;
    		for(j=0;j<N;j++){

    			at[t][i] += at[t-1][j]*A[j][i];

    		}
    		at[t][i] *= B[i][ob[t]];
    		ct[t] += at[t][i];
    	}
    	ct[t] = 1/ct[t];
    	for(i=0;i<N;i++){

    		at[t][i] *= ct[t];
    	}
    }
}

void b_pass(long double **bt, long double *ct, int *ob, int T, long double **A, long double **B, int N){

	int i,j,t;

	for(i=0;i<N;i++){

		bt[T-1][i]=ct[T-1];
	}

	for(t=T-2;t>=0;t--){

		for(i=0;i<N;i++){

			bt[t][i] = 0;
			for(j=0;j<N;j++){

				bt[t][i] += A[i][j]*B[j][ob[t+1]]*bt[t+1][j];
			}
			bt[t][i] *= ct[t];
		}
	}
}

void g_pass(struct gs *gs, long double **at, long double **bt, int *ob, int T, long double **A, long double **B, int N){

	int i,j,t;
	long double denom;

	for(t=0;t<T-1;t++){

		denom = 0;
		for(i=0;i<N;i++){

			for(j=0;j<N;j++){

				denom += at[t][i]*A[i][j]*B[j][ob[t]]*bt[t+1][j];
			}
		}

		for(i=0;i<N;i++){
			gs[t].gsi[i] = 0;

			for(j=0;j<N;j++){

				gs[t].gsij[i][j] = (long double )(at[t][i]*A[i][j]*B[j][ob[t+1]]*bt[t+1][j])/denom;
				gs[t].gsi[i] += gs[t].gsij[i][j];
			}
		}
	}

	//Special case for gs[T-1][i]
	denom = 0;
	for(i=0;i<N;i++){

		denom += at[T-1][i];
	}

	for(i=0;i<N;i++){

		gs[T-1].gsi[i]=(at[T-1][i])/denom;
	}

}

void re_ABP(struct gs *gs, long double **at, long double **bt, int *ob, int T, long double **A, long double **B, long double **P, int N, int M){

	int i,j,t;
	long double numer, denom;

	//re-estimate P
	for(i=0;i<N;i++){

		P[0][i] = gs[0].gsi[i];
	}

	//re-estimate A
	for(i=0;i<N;i++){

		for(j=0;j<N;j++){

			numer = 0;
			denom = 0;
			for(t=0;t<=T-2;t++){

				numer += gs[t].gsij[i][j];
				denom += gs[t].gsi[i];
			}
			A[i][j] = numer/denom;
		}
	}

	//re-estimate B
	for(i=0;i<N;i++){

		for(j=0;j<M;j++){

			numer = 0;
			denom = 0;
			for(t=0;t<=T-1;t++){

				if(ob[t]==j){
					numer += gs[t].gsi[i];
				}

				denom += gs[t].gsi[i];
			}
			B[i][j] = numer/denom;
		}
	}
}

void compute_po(long double *logProb, long double *ct, int T){

	int i;

	*logProb = 0;
	for(i=0;i<T;i++){

		*logProb += log(ct[i]);
	}
	*logProb = -(*logProb);
}

struct lambda hmm_traning(struct lambda lm, long double *logProb){

	//Training the model

	int i = 0;
	long double logProbOld, **temp, **A_temp, **B_temp, **P_temp;

	struct gs *gs = lm.gs;
	long double **at = lm.at;
	long double **bt = lm.bt;
	long double *ct = lm.ct;
	long double **A = lm.A;
	long double **B = lm.B;
	long double **P = lm.P;
	int *ob = lm.ob;
	int T = lm.T;
	int N = lm.N;
	int M = lm.M;

	struct lambda triple;

    A_temp = (long double **)malloc(sizeof(long double*)*N);
    B_temp = (long double **)malloc(sizeof(long double*)*N);
    P_temp = (long double **)malloc(sizeof(long double*)*1);


    for(i=0;i<N;i++){

        A_temp[i] = (long double *)malloc(sizeof(long double)*N);
        bzero(A_temp[i],sizeof(long double)*N);

        B_temp[i] = (long double *)malloc(sizeof(long double)*M);
        bzero(B_temp[i],sizeof(long double)*M);
    }

    P_temp[0] = (long double *)malloc(sizeof(long double)*N);
    bzero(P_temp[0],sizeof(long double)*N);

    i = 0;
    triple.A = A;
    triple.B = B;
    triple.P = P;

	while(i < 1000 ){

		a_pass(at, ct, ob, T, A, B, P, N);
		b_pass(bt, ct, ob, T, A, B, N);
		g_pass(gs, at, bt, ob, T, A, B, N);

		if(i==0){

			compute_po(logProb, ct,T);
			logProbOld = *logProb;
			D("[DEBUG] First prob: %Lf", *logProb);

		}

		re_ABP(gs, at, bt, ob,T, A_temp, B_temp, P_temp, N, M);
		a_pass(at, ct, ob, T, A_temp, B_temp, P_temp, N);
		compute_po(logProb, ct,T);

		D("[DEBUG] prob temp: %Lf", *logProb);

		if(*logProb>logProbOld){

			logProbOld = *logProb;

			temp = A;
			A = A_temp;
			A_temp = temp;
			triple.A = A;

			temp = B;
			B = B_temp;
			B_temp = temp;
			triple.B = B;

			temp = P;
			P = P_temp;
			P_temp = temp;
			triple.P = P;

			i++;

		}else{

			*logProb = logProbOld;

		    for(i=0;i<N;i++){

		        free(A_temp[i]) ;
		        free(B_temp[i]);

		    }

		    free(P_temp[0]);
		    free(A_temp);
		    free(B_temp);
		    free(P_temp);
		    break;
		}
	}

	return triple;
}

void hmm_generator(struct lambda lm, int seq, int *output_seq){

	int i,j,s, cur_value, check = 0;
	int N = lm.N;
	int M = lm.M;

	long double prob_pie = 0;
	long double **A = lm.A;
	long double **B = lm.B;
	long double **P = lm.P;

	//starting point

	long double rnd_value = (long double) rand()/(RAND_MAX);

	for(i=0;i<N;i++){

		prob_pie += P[0][i];
		if(rnd_value<=prob_pie){
			cur_value = i;
			check = 1;
			break;
		}
	}

	if(check==0){
		D("something is wrong!!!");
	}

	for(s=0;s<seq;s++){

		prob_pie = 0;
		rnd_value = (long double) rand()/(RAND_MAX);

		for(j=0;j<M;j++){

			if(B[cur_value][j]==0){
				continue;

			}else{
				prob_pie += B[cur_value][j];

				if(rnd_value<=prob_pie){
					output_seq[s] = j;
					//D("OUTPUT SEQUENCE IS: %d", output_seq[s]);
					break;
				}
			}

		}

		prob_pie = 0;
		rnd_value = (long double) rand()/(RAND_MAX);

		for(j=0;j<N;j++){

			if(A[cur_value][j]==0){
				continue;

			}else{
				prob_pie += A[cur_value][j];

				if(rnd_value<=prob_pie){
					cur_value = j;
				}

			}
		}
	}
}

int main(){

	srand(time(NULL));

	int i, z ,j = 0, ch, seq = 1000;

	int N_size = 10, M_size = 1440, T_size = 1000;
	int N_time = 10, M_time = 5000, T_time = 1000;
	int N_br = 10, M_br, T_br;

    int *out_size, *out_time, *time_map, *out_br;

    out_size = (int *)malloc(sizeof(int)*seq);
    out_time = (int *)malloc(sizeof(int)*seq);
    out_br = (int *)malloc(sizeof(int)*seq);
    time_map = (int *)malloc(sizeof(int));

	char *file_path = "results_input.txt";
	char *file_path_out = "results_output.txt";
	char *dev_name_sniff = "eno1"; //define interface for recoding real IP traffic
	char *dev_name_send = "eno1";  //define interfece
	char *dst_ip = "10.0.0.5";
	char **sendbuff;

    struct lambda pkt_size, pkt_time, br_time, temp;
    struct br_vls bord;
    long double logProb = 0;

    pkt_size = lm_init(N_size, T_size, M_size);
    pkt_time = lm_init(N_time, T_time, M_time);

    rnd_lm(pkt_size);
    rnd_lm(pkt_time);

    bord = rnd_seq(pkt_size, pkt_time, dev_name_sniff);

    /*
     * Creating border time sequence and border time observation
     * toDO put this code in function
     */

    ch = 0;
	for(i=0;i<bord.j;i++){
		if(i==0){
			time_map[i] = bord.br[i];
			j++;
		}else{
			for(z=0;z<j;z++){
				if(bord.br[i]==time_map[z]){
					ch = 1;
					break;
				}
			}

			if(ch==0){
				time_map = (int *)realloc(time_map,(sizeof(int)*(j+1)));
				time_map[j] = bord.br[i];
				j++;
			}
			ch=0;
		}
	}
	M_br = j;
	T_br = bord.j;

	br_time = lm_init(N_br, T_br, M_br);
	rnd_lm(br_time);

	for(i=0;i<br_time.T;i++){
		for(j=0;j<br_time.M;j++){
			if(bord.br[i]==time_map[j]){
				br_time.ob[i]=j;
				break;
			}
		}
	}

	/*
	 * End of border time
	 */

	hmm_generator(pkt_size, seq, out_size);

	temp = hmm_traning(pkt_size, &logProb);
	pkt_size.A = temp.A;
	pkt_size.B = temp.B;
	pkt_size.P = temp.P;

	temp = hmm_traning(pkt_time, &logProb);
	pkt_time.A = temp.A;
	pkt_time.B = temp.B;
	pkt_time.P = temp.P;

	temp = hmm_traning(br_time, &logProb);
	br_time.A = temp.A;
	br_time.B = temp.B;
	br_time.P = temp.P;

	sendbuff = init_pkt(M_size, dst_ip, dev_name_send);

	D("Matrix B:");
	write_lambda(pkt_time.B,pkt_time.N,pkt_time.M);

	for(i=0;i<10;i++){

		hmm_generator(pkt_size, seq, out_size);
		hmm_generator(pkt_time, seq, out_time);
		hmm_generator(br_time, seq, out_br);

		for(j=0;j<seq;j++){
			out_br[j] = time_map[out_br[j]];
		}

		ch = sendraw(out_size, out_time, out_br, seq, pkt_time.M, dev_name_send, sendbuff);

		if(ch == 1){
			D("SEQ SUCCESSFULLY SENT %d",i);
		}else{
			D("ERROR");
		}
	}

	int *par_size, *par_time, *par_br;

	par_size = (int *)malloc(sizeof(long double)*pkt_size.M);
	par_time = (int *)malloc(sizeof(long double)*pkt_time.M);
    par_br = (int *)malloc(sizeof(long double)*br_time.M);
    bzero(par_size,sizeof(long double)*pkt_size.M);
    bzero(par_time,sizeof(long double)*pkt_time.M);
    bzero(par_br,sizeof(long double)*br_time.M);


    result_pars(pkt_size.ob,pkt_size.T,par_size);
    result_pars(pkt_time.ob,pkt_time.T,par_time);
    result_pars(br_time.ob,br_time.T,par_br);

	write_seq(par_size, pkt_size.M, par_time, pkt_time.M, par_br, br_time.M, time_map, br_time.M, pkt_size.T, br_time.T, file_path);

	bzero(par_size,sizeof(long double)*pkt_size.M);
	bzero(par_time,sizeof(long double)*pkt_time.M);
	bzero(par_br,sizeof(long double)*br_time.M);

    result_pars(out_size,seq,par_size);
    result_pars(out_time,seq,par_time);
    result_pars(out_br,seq,par_br);

    write_seq(par_size, pkt_size.M, par_time, pkt_time.M, par_br, br_time.M, time_map, br_time.M, seq, seq, file_path_out);

	//end of the training

	D("log probability %Lf", logProb);
	D("Matrix P:");
	write_lambda(pkt_time.P,1,pkt_time.N);
	D("Matrix A:");
	write_lambda(pkt_time.A,pkt_time.N,pkt_time.N);
	//D("Matrix B:");
	//write_lambda(pkt_time.B,pkt_time.N,pkt_time.M);

	return 0;
}
