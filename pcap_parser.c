/*
 * Copyright (C) 2017 Hasan Redzovic, Aleksandra Smiljanic. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <hmm-ip-gen.h>

extern long double time_ms_sub(struct timespec a, struct timespec b);

struct br_vls sniffer(char *dev_name, int T, int *ob_size, int *ob_time, int M_size, int M_time){

	int i = 0, j = 0, br_vl = 0;
	char *dev;
	long double tm_sub;

	struct timespec cur, past;
	struct br_vls brod;
	brod.br = (int *)malloc(sizeof(int));

	// PCAP variables
	pcap_t *handle;			/* Session handle */
	char errbuf[PCAP_ERRBUF_SIZE];	/* Error string */
	struct pcap_pkthdr header;	/* The header that pcap gives us */
	const u_char *packet;		/* The actual packet */

	/* Define the device */

	if(dev_name && !dev_name[0]){
		dev = pcap_lookupdev(errbuf);
	}else{
		dev = dev_name;
	}

	if (dev == NULL) {
		fprintf(stderr, "Couldn't find default device: %s\n", errbuf);
	}

	/* Open the session in promiscuous mode */
	handle = pcap_open_live(dev, BUFSIZ, 1, 1500, errbuf);

	if (handle == NULL) {
		D("Couldn't open device %s: %s\n", dev, errbuf);
	}

	clock_gettime(CLOCK_REALTIME, &past);

	while(i<T){

		while(1){
			packet = pcap_next(handle, &header);
			if(packet == NULL)
				continue;
			if((header.len - 60) < M_size && (header.len - 60) >= 0){
				break;
			}else{
				header.len = 0;
			}

		}

		clock_gettime(CLOCK_REALTIME, &cur);
		tm_sub = time_ms_sub(cur, past);

		if(tm_sub>=M_time){
			ob_time[i] = M_time - 1;
			brod.br = (int *)realloc(brod.br,(sizeof(int)*j+1));
			br_vl = (int)tm_sub/M_time;

			if(br_vl<0){
				brod.br[j] = (INT_MAX - 1)/M_time;
			}else{
				brod.br[j] = br_vl;
			}

			j++;

		}else if(tm_sub<0){
			ob_time[i] = 0;
		}else{
			ob_time[i] = (int)tm_sub;
		}

		ob_size[i] = header.len - 60;
		//D("TRANING SEQ SIZE: %d AND TIME: %d", ob_size[i], ob_time[i]);
		past = cur;
		i++;
	}

	brod.j = j;

	for(i=0;i<j;i++){
		D("BOREDER: %d", brod.br[i]);
	}

	/* And close the session */
	pcap_close(handle);

	return brod;
 }



