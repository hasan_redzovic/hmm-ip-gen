/*
 * Copyright (C) 2017 Hasan Redzovic, Aleksandra Smiljanic. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */


#ifndef HMM_IP_GEN_H_
#define HMM_IP_GEN_H_

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <sys/time.h>
#include <pcap.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/ether.h>
#include <linux/if_packet.h>
#include <sys/ioctl.h>
#include <time.h>
#include <limits.h>

#define MY_DEST_MAC0	0x00
#define MY_DEST_MAC1	0x0c
#define MY_DEST_MAC2	0x29
#define MY_DEST_MAC3	0x6c
#define MY_DEST_MAC4	0xb6
#define MY_DEST_MAC5	0x77

#define BUF_SIZ		1500


struct br_vls{
	int *br;
	int j;
};

#define D(_fmt, ...)				\
	do {							\
		struct timeval _t0;				\
		gettimeofday(&_t0, NULL);			\
		fprintf(stderr, "%03d.%06d %s [%d] " _fmt "\n",	\
		    (int)(_t0.tv_sec % 1000), (int)_t0.tv_usec,	\
		    __FUNCTION__, __LINE__, ##__VA_ARGS__);	\
        } while (0)


struct br_vls sniffer(char *dev_name, int T, int *ob_size, int *ob_time, int M_size, int M_time);
int sendraw(int *out_seq, int *out_time, int *out_br, int seq, int M,  char *dev, char **send_ar);
char **init_pkt(int pkt_range, char *dst_ip, char *dev);
struct timespec timespec_sub(struct timespec a, struct timespec b);
struct timespec timespec_add(struct timespec a, struct timespec b);
long double time_ms_sub(struct timespec a, struct timespec b);
struct timespec wait_time(struct timespec ts);


#endif /* HMM_H_ */


