EXECUTABLES = hmm-ip-gen

BUILDDIR = .
SOURCEDIR = .

_DEPS = hmm-ip-gen.o pcap_parser.o sendraw.o time_fun.o
DEPS = $(patsubst %,$(BUILDDIR)/%,$(_DEPS))

UNAME := $(shell uname)

ifeq ($(UNAME), FreeBSD)
	CC=cc
else ifeq ($(UNAME), Linux)
	CC=gcc
endif

CFLAGS = -I$(SOURCEDIR) -g -Wall -O0
LFLAGS = -lm -lpcap

all: $(EXECUTABLES)

$(EXECUTABLES): % : $(BUILDDIR)/%.o $(DEPS)
	$(CC) -o $@ $^ $(CFLAGS) $(LFLAGS)

$(BUILDDIR)/%.o: $(SOURCEDIR)/%.c
	$(CC) -c -o $@ $< $(CFLAGS)

clean:
	rm -f $(EXECUTABLES) $(BUILDDIR)/*.o *.log
