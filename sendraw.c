/*
 * Copyright (C) 2017 Hasan Redzovic, Aleksandra Smiljanic. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "hmm-ip-gen.h"
unsigned short csum(unsigned short *buf, int nwords)
{
    unsigned long sum;
    for(sum=0; nwords>0; nwords--)
        sum += *buf++;
    sum = (sum >> 16) + (sum &0xffff);
    sum += (sum >> 16);
    return (unsigned short)(~sum);
}

char **init_pkt(int pkt_range, char *dst_ip, char *dev_name){

	int i, j, sockfd, tx_len = 0;
	char **sendbuf, *ip_dst, ifName[IFNAMSIZ];
	struct ifreq if_idx;
	struct ifreq if_mac;
	struct ifreq if_ip;
	struct ether_header *eh;
	struct iphdr *iph;
	struct udphdr *udph;

	sendbuf = (char **)malloc(sizeof(char *)*pkt_range);
	for(i=0;i<pkt_range;i++){
		sendbuf[i] = (char *)malloc(sizeof(char)*BUF_SIZ);
	}

	/* Get interface name */

	strcpy(ifName, dev_name);

	/* Open RAW socket to send on */

	if ((sockfd = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1) {
		perror("socket");
	}

	/* Get the index of the interface to send on */

	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0)
		perror("SIOCGIFINDEX");

	/* Get the MAC address of the interface to send on */

	memset(&if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFHWADDR, &if_mac) < 0)
		perror("SIOCGIFHWADDR");

	/* Get the IP address of the interface to send on */

	memset(&if_ip, 0, sizeof(struct ifreq));
	strncpy(if_ip.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFADDR, &if_ip) < 0)
		perror("SIOCGIFADDR");

	/* Get the IP destination address */

	ip_dst = dst_ip;

	for(i=0;i<pkt_range;i++){

		tx_len = 0;

		eh = (struct ether_header *) sendbuf[i];
		iph = (struct iphdr *) (sendbuf[i] + sizeof(struct ether_header));
		udph = (struct udphdr *) (sendbuf[i] + sizeof(struct iphdr) + sizeof(struct ether_header));

		/* Construct the Ethernet header */
		memset(sendbuf[i], 0, BUF_SIZ);
		/* Ethernet header */
		eh->ether_shost[0] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[0];
		eh->ether_shost[1] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[1];
		eh->ether_shost[2] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[2];
		eh->ether_shost[3] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[3];
		eh->ether_shost[4] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[4];
		eh->ether_shost[5] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[5];
		eh->ether_dhost[0] = MY_DEST_MAC0;
		eh->ether_dhost[1] = MY_DEST_MAC1;
		eh->ether_dhost[2] = MY_DEST_MAC2;
		eh->ether_dhost[3] = MY_DEST_MAC3;
		eh->ether_dhost[4] = MY_DEST_MAC4;
		eh->ether_dhost[5] = MY_DEST_MAC5;
		/* Ethertype field */
		eh->ether_type = htons(ETH_P_IP);
		tx_len += sizeof(struct ether_header);

		/*Construct the IP header */

		iph->ihl = 5;
		iph->version = 4;
		iph->tos = 16; // Low delay
		iph->id = htons(54321);
		iph->ttl = IPDEFTTL; // hops
		iph->protocol = IPPROTO_UDP; // UDP
		iph->saddr = inet_addr(inet_ntoa(((struct sockaddr_in *)&if_ip.ifr_addr)->sin_addr)); //source IP device
		iph->daddr = inet_addr(ip_dst); // iph->saddr = inet_addr("192.168.0.112");
		tx_len += sizeof(struct iphdr);

		/* Construct the UDP header: */

		udph->source = htons(3423);
		udph->dest = htons(5342);
		udph->check = 0; // skip
		tx_len += sizeof(struct udphdr);

		for(j=0;j<i+18;j++){
			sendbuf[i][tx_len++] = 0xde;
		}

		/* Length of UDP payload and header */
		udph->len = htons(tx_len - sizeof(struct ether_header) - sizeof(struct iphdr));
		/* Length of IP payload and header */
		iph->tot_len = htons(tx_len - sizeof(struct ether_header));
		/* Calculate IP checksum on completed header */
		iph->check = csum((unsigned short *)(sendbuf[i]+sizeof(struct ether_header)), sizeof(struct iphdr)/2);

	}

	return sendbuf;

}

int sendraw(int *out_seq, int *out_time, int *out_br, int seq, int M, char *dev, char **send_ar){

	int i, j = 0, sockfd;
	char  ifName[IFNAMSIZ];

	struct ifreq if_idx;
	struct sockaddr_ll socket_address;
	struct timespec wait;
	wait.tv_sec = 0;

	strcpy(ifName, dev);

	/* Open RAW socket to send on */

	if ((sockfd = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1) {
	    perror("socket");
	}

	/* Get the index of the interface to send on */

	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0)
	    perror("SIOCGIFINDEX");


	/* Index of the network device */
	socket_address.sll_ifindex = if_idx.ifr_ifindex;
	/* Address length*/
	socket_address.sll_halen = ETH_ALEN;
	/* Destination MAC */
	socket_address.sll_addr[0] = MY_DEST_MAC0;
	socket_address.sll_addr[1] = MY_DEST_MAC1;
	socket_address.sll_addr[2] = MY_DEST_MAC2;
	socket_address.sll_addr[3] = MY_DEST_MAC3;
	socket_address.sll_addr[4] = MY_DEST_MAC4;
	socket_address.sll_addr[5] = MY_DEST_MAC5;

	/* Send packet */

	for(i=0;i<seq;i++){

		if(out_time[i]<M-1){
			wait.tv_nsec=out_time[i];
			wait_time(wait);
		}else{
			wait.tv_nsec=out_br[j]*M + out_time[i];
			wait_time(wait);
			j++;
		}
		if (sendto(sockfd, send_ar[out_seq[i]], out_seq[i]+60, 0, (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll)) < 0){
			printf("Send failed\n");
		}
		i++;
	}
	close(sockfd);
	return 1;
}

