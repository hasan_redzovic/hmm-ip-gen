/*
 * Copyright (C) 2017 Hasan Redzovic, Aleksandra Smiljanic. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */



#include "hmm-ip-gen.h"

__inline struct timespec timespec_sub(struct timespec a, struct timespec b){

	struct timespec ret = { a.tv_sec - b.tv_sec, a.tv_nsec - b.tv_nsec };
	if (ret.tv_nsec < 0) {
		ret.tv_sec--;
		ret.tv_nsec += 1000000000;
	}
	return ret;
}

__inline long double time_ms_sub(struct timespec a, struct timespec b){

	long double ret = 0;

	struct timespec sub = { a.tv_sec - b.tv_sec, a.tv_nsec - b.tv_nsec };
	if (sub.tv_nsec < 0) {
		sub.tv_sec--;
		sub.tv_nsec += 1000000000;
	}
	ret = (long double)(sub.tv_sec*1000000000 + sub.tv_nsec);
	return ret;
}

__inline struct timespec timespec_add(struct timespec a, struct timespec b){

	struct timespec ret = { a.tv_sec + b.tv_sec, a.tv_nsec + b.tv_nsec };
	if (ret.tv_nsec >= 1000000000) {
		ret.tv_sec++;
		ret.tv_nsec -= 1000000000;
	}
	return ret;
}


extern struct timespec timespec_sub(struct timespec a, struct timespec b);
extern struct timespec timespec_add(struct timespec a, struct timespec b);

struct timespec wait_time(struct timespec ts){

	struct timespec cur, next;

	clock_gettime(CLOCK_REALTIME, &next);
	next = timespec_add(next,ts);

	for (;;) {
		struct timespec w;
		clock_gettime(CLOCK_REALTIME, &cur);
		w = timespec_sub(next, cur);
		if (w.tv_sec < 0)
			break;
	}
	return cur;
}

